const express = require('express')
const app = express()
// 引入user路由模块组
const userRouter = require('./user')
app.use('/user', userRouter)
// 引入home路由模块组
const homeRouter = require('./home')
app.use('/home',homeRouter)




app.listen(8848, () => console.log('启动成功,端口号是8848'))

