// db.js文件
const mysql = require('mysql');
const connection = mysql.createConnection({
    host: "localhost", // 据库的 IP 地址
    port: "3306", // 数据库的端口
    user: "root", // 登录数据库的账号
    password: "123456", // 登录数据库的密码
    database: "stu", // 指定要操作哪个数据库
});

connection.connect((err) => {
    if (err) throw err;
    console.log('成功链接数据库');
});

module.exports = connection;
